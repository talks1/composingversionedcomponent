# Composing Versioned Components

## The Problem Space

We have a system which can be considered a composition of individual components.

We want to upgrade the individual components to improve the system.

We want to do so in a way which allows verification of the new component concurrently with the old component.


##  Solution Form

***What is the solution going to entail***

We would seem to need
- components to interact with each other through a queue/event/persistent log system
- a way to route events to the new component
- the component that does the routing needs to be able to get visibility of the new components


## Realistic Scenario

An application needs to allow content to be uploaded and meta to be associated with the content and then some process follows
Ideally this application can be composed of 3 independent aspects, 
1. uploading and 
2. associating meta data
3. the subsequent process

We want
```
process *composed with* upload *composed with* metadata (content)
```

We want to be able to version all 3 components so that we have at any one time
- upload1
- upload2
- metadata1
- metadata2
- process1
- process2

We will use the term *Artifact* for the thing which associated the content and metadata.

An artifact is created.
In any order content can be uploaded or meta data can be defined or the process can be initiated.

```
Artefact
-> Upload using some version
-> Meta Data using some version
-> Process using some version
```

## Solution Proposal

Entity is created onto an event queue.
Dispatcher pull from queue.
Dispatcher looks at type of Entity
Dispatcher queries components who can process the Entity.
Components return their version number.
Dispatcher chooses a version and invokes component with the Entity.

WORK IN PROGRESS